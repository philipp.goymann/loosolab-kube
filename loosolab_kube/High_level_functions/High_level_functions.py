#!/usr/bin/env python3
import sys
import uuid

#self import
import loosolab_kube.Kube_objects.Job as Job
import loosolab_kube.Kube_objects.PVC as PVC
import loosolab_kube.Kube_objects.Secret as Secret
from loosolab_kube.Utils.Logger import KubernetesLogger
from loosolab_kube.Utils.utils import Utils
from loosolab_s3.s3_functions import Loosolab_s3


class High_level_functions(Utils):
    def __init__(self, tool_name="Kube High_level_functions", level=3):
        self.logger = KubernetesLogger(tool_name=tool_name, level=level)
        self.logger.begin()

    def send_files_to_pvc(self, file_list, name_pvc, namespace, s3secret, s3username, secretname='mpis3',
            s3endpoint='https://s3.mpi-bn.mpg.de', bucket_name ='', prefix_for_bucket = None):

        #create required instances
        job = Job(namespace) 
        pvc = PVC(namespace)
        secret = Secret(namespace)
        job = Job(namespace)
        
        #check if file is given es list
        file_list = self.check_if_list_obj(file_list, message='Files')
        self.logger.debug('List of files: ' + str(file_list))
        #check if secret exists
        if not secret.check_if_secret_ex(secretname):
            self.logger.error('Secret ' + secretname + ' didn\'t exists!')
            raise Exception

        self.logger.debug('The secret you chose containes the following data: ' + str(secret.get_secret_data(secretname)))

        #check if pvc exists
        if not pvc.check_if_pvc_ex(name_pvc):
            self.logger.error('PVC ' + name_pvc + ' didn\'t exists!')
            raise Exception
        
        credentials = {
        "endpoint":s3endpoint,
        "key":s3username,
        "secret": s3secret,
        "signature":"s3v4"
        }
        self.logger.debug('Run with these s3 credentials: ' + str(credentials))
        s3 = Loosolab_s3(credentials)

        #create unique bucket
        if prefix_for_bucket == None:
            bucket_name += str(uuid.uuid4()).replace('-','')
            if len(bucket_name) >= 63:
                bucket_name = bucket_name[:63-len(bucket_name)]
            s3.create_s3_bucket(bucket_name, name_addition=True)
        else:
            #if bucket must begin with 
            prefix_for_bucket += str(uuid.uuid4()).replace('-','')
            bucket_name = prefix_for_bucket
            if len(bucket_name) >= 63:
                bucket_name = bucket_name[:63-len(bucket_name)]
            s3.create_s3_bucket(bucket_name, name_addition=True)
        self.logger.debug('Choosen Bucket name is: ' + bucket_name)
        #upload files
        s3.upload_s3_objects(bucket_name, file_list, compare=True)
        
        #create body vor job which will be downloaded later own on the cluster
        volumes_mounts = job.volume_mounts('exchange', '/exchanger')

        env_list = []
        env_list.append(job.env_variables('endpoint', 'endpoint', secretname))
        env_list.append(job.env_variables('password', 'password', secretname))
        env_list.append(job.env_variables('username', 'username', secretname))

        comandline = 'cd /exchanger; python3 ../app/s3.py -username $(username) -password $(password) \
                                -endpiont $(endpoint) -download_bucket ' +  bucket_name + ' -download_files ' + ' '.join(file_list)
        
        self.logger.debug('Runnig with hte comandline: ' + comandline)
        
        container = job.container('s3-downloader', 'pgoymann/s3loader:version5', comand_for_container=['-c', comandline], comand=['/bin/sh'],
        volume_mounts=[volumes_mounts], env_list=env_list)

        volume = job.volumes('exchange', name_of_PVC=name_pvc )
        name_of_Job = 'send-files-to-pvc-' + str(uuid.uuid4()).replace('-','')

        self.logger.debug('Name of Job is: ' + name_of_Job)
        body_of_Job = job.build_Job_obj(name_of_Job, [container], volumes=[volume])

        job.create_Job(body_of_Job)
        job.watch_job_util_it_finished(name_of_Job)
        job.delete_Job(name_of_Job)
        s3.emptie_s3_buckets(bucket_name, delete_bucket = True)

        
