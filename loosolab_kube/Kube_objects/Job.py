#!/usr/bin/env python3
import kubernetes
from kubernetes import client, config, watch
from kubernetes.client.rest import ApiException

# self import
from .Kube import Kube
from loosolab_kube.Utils.utils import Utils

class Job(Kube, Utils):

    def __init__(self, namespace, level=3, config_file= False):
        """
            Checks the namespace, loads the Api instace form main Kube  and create an empty Job object.
        """
        super().__init__(namespace,  tool_name="Job", level=level, config_file= config_file)
        self.w = watch.Watch()
        self.template = client.V1PodTemplate()
        self.template =client.V1PodTemplateSpec()
        self.body_of_Job = client.V1Job(api_version="batch/v1", kind="Job", metadata = client.V1ObjectMeta(namespace = self.namespace),
            status = client.V1JobStatus(),
                spec = client.V1JobSpec(template=self.template))

    #--------------------------------------------------------------------------------------------------------#

    def build_Job_obj(self, name_of_Job, container, init_container=None,volumes=[], backoff_limit=3,ttl_seconds_after_finished=600):
        """
            Creates an Job object.
                required:
                    name_of_Job
                    container = Container objects create them with the container Function must be given as list
                optional:
                    init_container = Create an Initcontainer for the Job
                    volumes = Volumes which should be mounted use the volumes Function

        """
        self.check_if_list_obj(container, message='Container')
        self.check_if_list_obj(init_container, message='InitContainer')
        self.check_if_list_obj(volumes, message='Volume')
        self.body_of_Job.metadata.name = name_of_Job
        if init_container == None:
            self.body_of_Job.spec.template.spec = client.V1PodSpec(containers = container, restart_policy='Never',volumes=volumes)
        else:
            self.body_of_Job.spec.template.spec = client.V1PodSpec(containers = container, init_containers = init_container, restart_policy='Never',volumes=volumes)
        self.body_of_Job.spec.backoff_limit = backoff_limit
        self.body_of_Job.spec.ttl_seconds_after_finished = ttl_seconds_after_finished

        return self.body_of_Job

    #--------------------------------------------------------------------------------------------------------#

    def check_if_Job_ex(self, name_of_Job):
        #Checks if Job exists returns a True or False
        try:
            api_response = self.BatchV1Api.list_namespaced_job(self.namespace, field_selector='metadata.name=' + name_of_Job)
        except  ApiException as e:
            self.logger.error('Cloud not list Job' + str(e))
            raise Exception
        if len(api_response.items) != 0:
            self.logger.info('Job {} exists!'.format(name_of_Job))
            return True
        else:
            self.logger.warning('Job {} didn\'t exists!'.format(name_of_Job))
            return False

    #--------------------------------------------------------------------------------------------------------#

    def create_Job(self, body_of_Job):
        #Creates a Job on the Cluster
        if self.check_if_Job_ex(name_of_Job = body_of_Job.metadata.name):
            self.logger.error('Job {} already exists!'.format(body_of_Job.metadata.name))
        try:
     
            self.BatchV1Api.create_namespaced_job(self.namespace, body_of_Job)
            self.logger.info('Created Job: ' + body_of_Job.metadata.name)
        except ApiException as e:
            self.logger.error('Cloud not create Job: ' + str(e))
            raise Exception

    #--------------------------------------------------------------------------------------------------------#

    def delete_Job(self, name_of_Job):
        #Deletes a Job on the Cluster
        if not self.check_if_Job_ex(name_of_Job):
            self.logger.error('Cloud not delted Job {} didn\'t exists!'.format(name_of_Job))
        try:#create new secret
            body = client.V1DeleteOptions(propagation_policy='Background')#need to delte pod and running job
            self.BatchV1Api.delete_namespaced_job(name_of_Job, self.namespace, body=body)
            self.logger.info('Deleted Job: '+ name_of_Job)
        except ApiException as e:
            self.logger.error('Deleted Job ERROR: ' + str(e))
            raise Exception

    #--------------------------------------------------------------------------------------------------------#
    
    def list_all_Jobs(self, full_information = False):
        try:
            jobs_of_namespace = self.BatchV1Api.list_namespaced_job(self.namespace, pretty=True)
            if full_information:
                self.logger.info('Returned description of all Jobs.')
                return jobs_of_namespace
            else:
                self.logger.info('Returned list of all Jobs names.')
                return [i.metadata.name for i in jobs_of_namespace.items]
        except ApiException as e:
            self.logger.error('Listing all Jobs failed: ' + str(e))
            raise Exception

    #--------------------------------------------------------------------------------------------------------#

    def watch_job_util_it_finished(self, name_of_Job):
        for event in self.w.stream(self.BatchV1Api.list_namespaced_job, namespace=self.namespace):
            self.logger.debug("Event: "  + event['type'] + str(event['object'].status.succeeded) + event['object'].metadata.name)
            if event['object'].status.succeeded == 1 and event['object'].metadata.name == name_of_Job:
                self.w.stop()
            if event['object'].status.failed != None:
                logger.warning('Their is someting wrong with the Job failed: ' + str(event['object'].failed)  + ' times!')
            if event['object'].status.failed == 4:
                logger.error('Their Job faild 4 times!')
                self.w.stop()
                return
        self.logger.info('Job ' + name_of_Job + 'has finished!')
