#!/usr/bin/env python3
import kubernetes
from kubernetes import client, config
from kubernetes.client.rest import ApiException
import sys
# self import

from .Kube import Kube
from loosolab_kube.Utils.utils import Utils

class Deployment(Kube, Utils):

    def __init__(self, namespace, level=3,tool_name = "Deployment", config_file= False):
        """
            init Class creates an empty Deployment object and checks the namespace
            The api instance gt loaded from the main Kube class.
        """
        super().__init__(namespace,  tool_name=tool_name, level=level, config_file= config_file)

        self.template = client.V1PodTemplate()
        self.template =client.V1PodTemplateSpec()
        self.body_of_Deployment = client.V1Deployment(api_version="apps/v1", kind="Deployment",
         metadata = client.V1ObjectMeta(namespace = self.namespace),
                spec = client.V1DeploymentSpec(selector = kubernetes.client.V1LabelSelector({'run':'test'}),template=self.template))

    #--------------------------------------------------------------------------------------------------------#

    def build_Deployment_obj(self, name_of_Deployment, container, match_labels_dict = {'run':'test'}, init_container=None,volumes=[],  replicas = 1):
        """
            required:
                name_of_Deployment
                container = use the the container function to create an container object
            optional:
                match_labels_dict = labels to bound on a Service object.
                init_container = add a InitContainer to the Deployment.
                volumes = add volumes to the DEployment use the volumes function to create Volumeobjects.
        """
        try:
            #check if obj are list objects
            self.check_if_list_obj(container, message='Container')
            self.check_if_list_obj(init_container, message='InitContainer')
            self.check_if_list_obj(volumes, message='Volumes')

            #add metadata
            self.body_of_Deployment.metadata.name = name_of_Deployment
            self.body_of_Deployment.metadata.namespace=self.namespace

            #if init container is set it is added to the Deployment description
            if init_container == None:
                self.body_of_Deployment.spec.template.spec = client.V1PodSpec(containers = container,volumes=volumes)
            else:
                self.body_of_Deployment.spec.template.spec = client.V1PodSpec(containers = container, init_containers = init_container,volumes=volumes)
            #add spec part
            self.body_of_Deployment.spec.template.metadata = kubernetes.client.V1ObjectMeta(labels = match_labels_dict)
            self.body_of_Deployment.spec.replicas = replicas
            self.body_of_Deployment.spec.selector = kubernetes.client.V1LabelSelector(match_labels = match_labels_dict)
            return self.body_of_Deployment
        except Exception as e:
            raise Exception('Could not build Deployment ' + str(e))

    #--------------------------------------------------------------------------------------------------------#

    def check_if_Deployment_ex(self, name_of_Deployment):
        """
            Checks if a Deployment exist in a given namespaces.
        """
        try:
            api_response = self.AppsV1Api.list_namespaced_deployment(self.namespace, field_selector='metadata.name=' + name_of_Deployment)
        except ApiException as e:
            self.logger.error('Could not list Deployment' + str(e))
            raise Exception('Could not list Deployment' + str(e))

        if len(api_response.items) != 0:
            self.logger.info('Deployment {} exists!'.format(name_of_Deployment))
            return True
        else:
            self.logger.warning('Deployment {} doesn\'t exists!'.format(name_of_Deployment))
            return False

    #--------------------------------------------------------------------------------------------------------#

    def create_Deployment(self, body_of_Deployment):
        """
            Sends a Deployment to the Cluster if it dosen't exists.
        """   
        if self.check_if_Deployment_ex(body_of_Deployment.metadata.name):
            self.logger.error('Deployment {} already exists!'.format(body_of_Deployment.metadata.name))
        try:
            self.AppsV1Api.create_namespaced_deployment(self.namespace, self.body_of_Deployment)
            self.logger.info('Created Deployment: ' + body_of_Deployment.metadata.name)
        except ApiException as e:
            self.logger.error('Could not create Deployment: ' + str(e))
            raise Exception('Could not create Deployment: ' + str(e))

    #--------------------------------------------------------------------------------------------------------#

    def delete_Deployment(self, name_of_Deployment):
        """
            Deletes a Deployment which runs on the Cluster.
        """
        if not self.check_if_Deployment_ex(name_of_Deployment):
            self.logger.error('Could not delete Deployment {} doesn\'t exist!'.format(name_of_Deployment))
        try:#create new secret
            self.AppsV1Api.delete_namespaced_deployment(name_of_Deployment, self.namespace)
            self.logger.info('Deleted Deployment: '+ name_of_Deployment)
        except ApiException as e:
            self.logger.error('Could not delete Deployment: ' + str(e))
            raise Exception('Could not delete Deployment: ' + str(e))

    #--------------------------------------------------------------------------------------------------------#
    
    def list_all_Deployments(self, full_information = False):
        try:
            deployments_of_namespace = self.AppsV1Api.list_namespaced_deployment(self.namespace, pretty=True)
            if full_information:
                self.logger.info('Returned description of all Deployments.')
                return deployments_of_namespace
            else:
                self.logger.info('Returned list of all Deployment names.')
                return [i.metadata.name for i in deployments_of_namespace.items]
        except ApiException as e:
            self.logger.error('Listing all Deployments failed: ' + str(e))
            raise Exception('Listing all Deployments failed: ' + str(e))

    #--------------------------------------------------------------------------------------------------------#
    def scale_deployment(self, name_of_Deployment, replica_count):  # replica count needs to be int
        """
            up or downscales a Deployment which runs on the Cluster.
            # https://github.com/kubernetes-client/python/issues/248
        """
        try:
            if not self.check_if_Deployment_ex(name_of_Deployment):
                self.logger.error('Could not scale Deployment, {} doesn\'t exist!'.format(name_of_Deployment))
                raise Exception('Could not scale Deployment, {} doesn\'t exist!'.format(name_of_Deployment))

            scale_body = {"spec":{"replicas": replica_count}}
            self.AppsV1Api.patch_namespaced_deployment(name_of_Deployment, self.namespace, scale_body) #(name, namespace, body, pretty=pretty, dry_run=dry_run, field_manager=field_manager, force=force)
            self.logger.info('Scaled Deployment: '+ name_of_Deployment + ' to ' + str(replica_count) + ' replicas.')
            
        except ApiException as e:
            self.logger.error('Could not scale Deployment: ' + str(e))
            raise Exception('Could not scale Deployment: ' + str(e))

    def update_container_image(self, name_of_Deployment, image_string, name_of_container="main-container"):
        """
            update the container image
            https://github.com/kubernetes-client/python/issues/347
        """
        try:
            if not self.check_if_Deployment_ex(name_of_Deployment):
                self.logger.error('Could not update container image, {} doesn\'t exist!'.format(name_of_Deployment))
                raise Exception('Could not update container image, {} doesn\'t exist!'.format(name_of_Deployment))

            # check image string? 
            image_body = {"spec":{"template":{"spec":{"containers":[{"name":name_of_container,"image":image_string}]}}}}
            self.AppsV1Api.patch_namespaced_deployment(name_of_Deployment, self.namespace, image_body, pretty=True)
            self.logger.info('Updated container image for '+ name_of_Deployment + ' to ' + image_string + ' .')
                
        except ApiException as e:
            self.logger.error('Could not update container image: ' + str(e))
            raise Exception('Could not update container image: ' + str(e))